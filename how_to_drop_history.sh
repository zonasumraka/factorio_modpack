#!/bin/bash
set -e
cd "$( dirname "${BASH_SOURCE[0]}" )"

# краткое описание процесса дропания хистори репозитория
# этот скрипт я ни разу не запускал, тут может что-то не сработать, короче кто не спрятался я не виноват

cd ..
mkdir novaya_repka
cd novaya_repka
git init
git remote add origin https://gitlab.com/zonasumraka/factorio_modpack
touch test.txt
git add . && git commit -m "Initial"
git push --set-upstream origin master --force